import math
import numpy as np
import time
import matplotlib.pyplot as plt
from PIL import Image
from scipy import misc
import cv2

#AstarMap=[[1, 0, 0, 0], [1,1,1, 0],[0,0,1, 0],[0,0,0, 0]]# make a DDA of size n*n #range from 0 to n-1
Path=list()
#1 is obstacle
#0 is clear path
#x or first index represents column or vertical dist
#x or 2nd index represents row or horizontal dist
AstarMap = []

        



class State():
    def __init__(self,x,y,parent):
        self.g=0
        self.h=0
        self.x=x
        self.y=y
        self.parent=parent
        #self.val=AstarMap[x][y]

    
    

    def getKids(self,n): #takes size of graph as argument
       self.Kids=list()
       Kids=[]
       
       if self.x<n-1 and AstarMap[self.x+1][self.y]==0:
          
          self.Kids.append(State(self.x+1,self.y,self))
          
          
       if self.x>0 and AstarMap[self.x-1][self.y]==0:
          self.Kids.append(State(self.x-1,self.y,self))
       if self.y<n-1 and AstarMap[self.x][self.y+1]==0:
          self.Kids.append(State(self.x,self.y+1,self))
       if self.y>0 and AstarMap[self.x][self.y-1]==0:
          self.Kids.append(State(self.x,self.y-1,self))
       

       #diagonal
       if self.y>0 and self.x>0 and AstarMap[self.x-1][self.y-1]==0 and AstarMap[self.x][self.y-1]==0 and AstarMap[self.x-1][self.y]==0:
          self.Kids.append(State(self.x-1,self.y-1,self))
       if self.y<n-1 and self.x>0 and AstarMap[self.x-1][self.y+1]==0 and AstarMap[self.x][self.y+1]==0 and AstarMap[self.x-1][self.y]==0:
          self.Kids.append(State(self.x-1,self.y+1,self))
       if self.y>0 and self.x<n-1 and AstarMap[self.x+1][self.y-1]==0 and AstarMap[self.x][self.y-1]==0 and AstarMap[self.x+1][self.y]==0:
          self.Kids.append(State(self.x+1,self.y-1,self))
       if self.y<n-1 and self.x<n-1 and AstarMap[self.x+1][self.y+1]==0 and AstarMap[self.x][self.y+1]==0 and AstarMap[self.x+1][self.y]==0:
          self.Kids.append(State(self.x+1,self.y+1,self))

       return self.Kids

    def display(self):
       print self.temp.x, self.temp.y, self.temp.g, self.temp.h,"parent" ,self.temp.parent.x, self.temp.parent.y, self.temp.parent.g         


class Astar:

    def __init__(self):
      self.pixvaluebw = []


    def getAstarMap(self,MapName):
        

        ## black is around 130-140, grey area is around 200-210, white is 240-255
        ##### obtain data from npy file and store it in an array
        #im=misc.imread(MapName)
        #grayIm=np.zeros((im.shape[0],im.shape[1]))

        start=time.clock()
        """
        for i in range(len(im)):
           for j in range(len(im[i])):
              if im[i][j][0]>240 and im[i][j][1]<10 and im[i][j][2]<10:
                 grayIm[i][j]=255
              else:                 
                 grayIm[i][j]=0.33*im[i][j][0]+0.33*im[i][j][1]+0.33*im[i][j][2]
        """
        #grayIm=[[0.33*im[i][j][0]+0.33*im[i][j][1]+0.33*im[i][j][2] for j in range(len(im[i]))] for i in range(len(im))]# need numpy average of rgb values
        grayIm=cv2.imread(MapName,0)
        end=time.clock()
        print "Intermediate time:" ,end-start
        
        data=np.array(grayIm)
        #data = np.load('map4_3x3meters.npy', mmap_mode = 'r')
        #print "data[1:5] :", data[0,1]
        pixvalue = [[0 for x in range(len(data[0])/10)] for x in range(len(data)/10)] # defining a 60*60 list  
        self.pixvaluebw = [[0 for x in range(len(data[0])/10)] for x in range(len(data)/10)]
        x = 0
        y = 0
        for i in range (0,len(data)-9,10):
           for j in range (0,len(data[i])-9,10):
               for k in range (0,10):
                   for l in range (0,10):
                      pixvalue[x][y] = pixvalue[x][y] + data[i+k][j+l] 
                      #print "data : ", data[i+k][j+l]        
               pixvalue[x][y] = pixvalue[x][y]/100
               #print "pixvalue : ",x," ",y," ", pixvalue[x][y] 
               if pixvalue[x][y]>210:  #threshold
                   self.pixvaluebw[x][y]=0#clear path
               else:
                   self.pixvaluebw[x][y]=1#obstacle
               #print "pixvalue : ",x," ",y," ", pixvalue[x][y]," ", pixvaluebw[x][y]
               y=y+1
           x=x+1
           y=0
        plt.imshow(self.pixvaluebw, cmap = "gray")
        #plt.show()

        return self.pixvaluebw
        


        
        
    def getPath(self,state):
        
        if state.parent==None:
            return [[state.x, state.y]]
        else:
            return self.getPath(state.parent)+[[state.x, state.y]]

    def compute(self, start_point, goal_point): #In SLAM co-ordinates x,y
      n=len(AstarMap[0])
      
      self.Open=list()
      self.Close=list()
      
      
      self.start=State(start_point[0]/100,start_point[1]/100,None) #input start and goal coord
      self.goal=State(goal_point[0]/100,goal_point[1]/100,None)
      if AstarMap[self.goal.x][self.goal.y]==1:
          print "such goal much wow"
          return
      
  
      self.current=self.start
      self.Open=self.Open+self.start.getKids(n)
      self.Close.append(self.start)
 
      self.minimum=0
      
      

      print "start loop"

      
      while self.current.x!=self.goal.x or self.current.y!=self.goal.y:
         j=0
         if len(self.Open)==0:
            print "no path found"
            break
         for self.temp in self.Open:
            if self.temp.x-self.temp.parent.x==0 or self.temp.y-self.temp.parent.y==0:
               self.temp.g = self.temp.parent.g + 1
               
            else:
               self.temp.g = self.temp.parent.g + 1.414
               
            self.temp.h = math.sqrt((self.temp.x-self.goal.x)**2+(self.temp.y-self.goal.y)**2)
            
            if j==0:
                
                self.minimum=self.temp.g+self.temp.h
                self.current=self.temp
                
            #print "min" self.minimum
            if self.minimum>(self.temp.g+self.temp.h):
                self.current=self.temp
                self.minimum=self.temp.g+self.temp.h
                
            j=j+1
            #print "iteration",j,"temp" ,self.temp.x, self.temp.y, self.temp.g, self.temp.h, self.temp.g+self.temp.h,"parent" ,self.temp.parent.x, self.temp.parent.y, self.temp.parent.g 
            

 
         
         #print "Current" ,self.current.x, self.current.y, "parent" ,self.current.parent.x, self.current.parent.y
         Kids=self.current.getKids(n)
         #print "KIDS size" ,len(Kids)
         #print "CLOSE size" , len(self.Close)
         #print "OPEN size", len(self.Open)
         #for k in self.Close:
             #print "close", k.x, k.y
 
         Kids1=list(Kids) #major bug solved  
         for self.temp in Kids1:# dont add kid if in close
            for self.tempClose in self.Close:
               if self.temp.x==self.tempClose.x and self.temp.y==self.tempClose.y  :
                 
                  Kids.remove(self.temp)
                  break #work aroud for close list is having repetitions
            for self.tempOpen in self.Open:
               if self.temp.x==self.tempOpen.x and self.temp.y==self.tempOpen.y  :
                  try:
                     Kids.remove(self.temp)
                  except:
                      pass 
                  break #work aroud for close list is having repetitions
         self.Open.remove(self.current)               
         self.Open=self.Open+Kids
         
         self.Close.append(self.current)
         

         #for self.temp in self.Open:
            #print "OPEN", self.temp.x, self.temp.y
          
      print "done"
         
      #print self.current.g+self.current.h #final cost
      Path= self.getPath(self.current)
      print Path
      PathUnzipped= zip(*Path) #only for graph
      
      #plt.scatter(*zip(*Path),color='gray')
      plt.scatter(PathUnzipped[1],PathUnzipped[0],color='gray')

      Path=[[(j*10+5)*10 for j in Path[i]] for i in range(len(Path))]
      print "scaled path", Path
      

      
     

      
      
def main():
  a=Astar()
  #start=time.clock()
  AstarMap=a.getAstarMap('map1.png')
  #end=time.clock()
  #print "TIME Map seconds", end-start
  #start=time.clock()
  a.compute()
  #end=time.clock()
  #print "TIME Astar seconds", end-start
  plt.show()
