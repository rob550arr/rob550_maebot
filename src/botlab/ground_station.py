import sys, os, time
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
 
import matplotlib.backends.backend_agg as agg

import pygame
from pygame.locals import *

#from lcmtypes import maebot_diff_drive_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t
from lcmtypes import rplidar_laser_t

from slam import *
from laser import *
from maps import *
from guidance import *

# Callling recently included files
from laser import *
from slam import *
from maps import *




# SLAM preferences
# CHANGE TO OPTIMIZE
USE_ODOMETRY = True
MAP_QUALITY = 5

# Laser constants
# CHANGE TO OPTIMIZE IF NECSSARY
DIST_MIN = 20; # minimum distance
DIST_MAX = 2000; # maximum distance

# Map constants
# CHANGE TO OPTIMIZE
MAP_SIZE_M = 6 # size of region to be mapped [m]
INSET_SIZE_M = 1.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 5 # depth of data points on map (levels of certainty)

# CONSTANTS
DEG2RAD = math.pi / 180
RAD2DEG = 180 / math.pi

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules

class MainClass:
  def __init__(self, width=640, height=480, FPS=10):
    pygame.init()
    self.width = width
    self.height = height
    self.screen = pygame.display.set_mode((self.width, self.height))

    ######MODE OF OPERATION#############
    self.modeOfOperation = 2
    #1 - Mapping with manual control
    #2 - Automated with manual plan
    #3 - Fully autonomous
    ####################################

    #Maebot state class variables
    self.currOdoPos = (0,0,0,0)           # store odometry x [mm], y [mm], theta [rad], utime
    self.currOdoVel = (0,0,0,0)
    self.accCurrOdoVel = [0,0,0]
    self.prevOdoVel = (0,0,0,0)
    #Lidar class variables
    self.lidarUtime = 0.0
    self.lidarNranges = 0
    self.lidarRanges = []
    self.lidarThetas = []
    self.lidarTimes = []
    self.lidarNintensities = 0
    self.lidarIntensities = []
    self.lidarRangesThetas = []
    self.lidarFlag = False
      
    # LCM Subscribe
    self.lc = lcm.LCM()
    lcmPoseSub = self.lc.subscribe("ODO_POSE_XYT", self.OdoPoseHandler)
    lcmVelSub = self.lc.subscribe("ODO_DXDTHETA", self.OdoVelocityHandler)
    lcmLidar = self.lc.subscribe("RPLIDAR_LASER", self.LidarHandler)    
    # NEEDS TO SUBSCRIBE TO OTHER LCM CHANNELS LATER!!!

    # Prepare Figure for Lidar
    self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch, 
    self.fig.patch.set_facecolor('white')
    self.fig.add_axes([0,0,1,1],projection='polar')
    self.ax = self.fig.gca()

    # Prepare Figures for control
    path = os.path.realpath(__file__)
    path = path[:-17] + "maebotGUI/"

    self.arrowup = pygame.image.load(path + 'fwd_button.png')
    self.arrowdown = pygame.image.load(path + 'rev_button.png')
    self.arrowleft = pygame.image.load(path + 'left_button.png')
    self.arrowright = pygame.image.load(path + 'right_button.png')
    self.resetbut = pygame.image.load(path + 'reset_button.png')
    self.arrows = [0,0,0,0]

    # PID Initialization - Change for your Gains!
    command = pid_init_t()
    command.kp = 0.0        
    command.ki = 0.0
    command.kd = 0.0
    command.iSat = 0.0 # Saturation of Integral Term. 
                           # If Zero shoudl reset the Integral Term
    self.lc.publish("GS_PID_INIT",command.encode())
  
    # Declare Laser, datamatrix and slam
    self.laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
    self.datamatrix = DataMatrix(**KWARGS) # handle map data
    self.slam = Slam(self.laser, **KWARGS) # do slam processing
    
    
  def OdoVelocityHandler(self,channel,data): 
    # IMPLEMENT ME !!!
    msg = odo_dxdtheta_t.decode(data)
    #print "currOdoVel", self.currOdoVel
    if self.currOdoVel[3]!=self.prevOdoVel[3]:
      self.currOdoVel = (msg.dxy, msg.dtheta, msg.dt, msg.utime)
      self.accCurrOdoVel[0] = self.accCurrOdoVel[0]+ self.currOdoVel[0]
      self.accCurrOdoVel[1] = self.accCurrOdoVel[1]+ self.currOdoVel[1]
      self.accCurrOdoVel[2] = self.accCurrOdoVel[2]+ self.currOdoVel[2]
      self.prevOdoVel = self.currOdoVel
 
  def OdoPoseHandler(self,channel,data):
    msg = odo_pose_xyt_t.decode(data)
    self.currOdoPos = (msg.xyt[0], msg.xyt[1], msg.xyt[2], msg.utime)
    
  def LidarHandler(self,channel,data): 
    msg = rplidar_laser_t.decode(data)
    self.lidarUtime = msg.utime
    self.lidarNranges = msg.nranges
    self.lidarRanges = []
    self.lidarThetas = []
    self.lidarTimes = []
    #for i in range(0,self.lidarNranges):
    #	    self.lidarRanges.extend([msg.ranges[i]])
    # 	    #Setting LIDAR ceiling at 1.5M. Max can be 2.00M
    #	    if self.lidarRanges[i] > 1.500: self.lidarRanges[i] = 1.500 
    #        self.lidarThetas.extend([msg.thetas[i]])
    #        self.lidarTimes.extend([msg.times[i]])
    
    self.lidarRanges = list(msg.ranges)
    self.lidarThetas = list(msg.thetas)
    self.lidarTimes = list(msg.times)
    
    
    self.lidarNintensities = msg.nintensities
    self.lidarIntensities = []
    
    self.lidarThetasDegrees = [int(i*180/math.pi) for i in self.lidarThetas]
    #print "LIDAR Size", self.lidarNranges
    
    #for i in range(self.lidarNintensities):
    #	    self.lidarIntensities.extend([self.lidarNintensities])
    self.lidarRangesProcessed = [0 for i in range(360)]
    for i in range(self.lidarNranges-1) :
      self.lidarRangesProcessed[self.lidarThetasDegrees[i]%360] = int(self.lidarRanges[i]*1000)	    
    
    self.lidarRangesThetas = [(self.lidarRangesProcessed[i], i) for i in range(360)]
    
    
    if self.lidarFlag == False:
      self.lidarFlag = True
      (y, x, theta) = self.slam.updateSlam(self.lidarRangesThetas, self.accCurrOdoVel)
      #print "accCurrOdoVel: ", self.accCurrOdoVel 
      #print "Origin", x,y,theta
      self.datamatrix.getRobotPos((y, x, theta), init=True)
      self.accCurrOdoVel = [0,0,0]

      if self.modeOfOperation == 3:
        self.guidance.generate_path([x,y,theta*math.pi/180])
    
    else:
      #Adding SLAM code
      #if time.time() - slamOldTime > 0.2 :
      #print "Lidar Update time:", self.lidarUtime
      #slamOldTime =  time.time()
      #print "Relative and Absolute positions", self.datamatrix.get_robot_rel(),self.datamatrix.get_robot_abs()
      #print "accCurrOdoVel: ", self.accCurrOdoVel 
      (y, x, theta) = self.slam.updateSlam(self.lidarRangesThetas, self.accCurrOdoVel)
      #print "Slam Position", (x, y, theta) 
      self.accCurrOdoVel = [0,0,0]
      self.datamatrix.getRobotPos((y, x, theta), init=False)
      #print "Relative and Absolute positions", self.datamatrix.get_robot_rel(),self.datamatrix.get_robot_abs()
      self.datamatrix.drawBreezyMap(self.slam.getBreezyMap())
      #self.datamatrix.drawMap(self.lidarRangesThetas)
      if self.modeOfOperation in [1,2]:
          self.datamatrix.saveImage()
          pass
          #self.slam.getmap(self.datamatrix)
          #self.slam.getmap(self.mapdata)
          #print self.datamatrix      
      #END of SLAM
      
    #Adding guidance
    if self.modeOfOperation in [2,3]:
        self.guidance.guide_and_command([x,y,theta*math.pi/180])
        #self.guidance.guide_and_command((self.currOdoPos[0],self.currOdoPos[1],self.currOdoPos[2]))
        
    #END OF GUIDANCE
      


  def MainLoop(self):
    pygame.key.set_repeat(1, 20)
    vScale = 0.5
    lidarOldTime = 0  
    # Prepare Text to Be output on Screen
    font = pygame.font.SysFont("DejaVuSans Mono",14)
    self.accCurrOdoVel = [0,0,0]
    #self.lc.handle()      
    #(y, x, theta) = self.slam.updateSlam(self.lidarRangesThetas, self.accCurrOdoVel)
    #print "accCurrOdoVel: ", self.accCurrOdoVel 
    #print "Origin", x,y,theta
    #self.datamatrix.getRobotPos((y, x, theta), init=True)
    #self.accCurrOdoVel = [0,0,0]
    #self.lc.handle()
    #(y, x, theta) = self.slam.updateSlam(self.lidarRangesThetas, self.accCurrOdoVel)
    #print "accCurrOdoVel: ", self.accCurrOdoVel 
    #print "Origin", x,y,theta
    #self.datamatrix.getRobotPos((y, x, theta), init=True)
    #slamOldTime =  time.time()

    # Guidance start
    if self.modeOfOperation in [2,3]:
      print "Initializing guidance"
      self.guidance = Guidance()
      #self.guidance.start()
      
      if self.modeOfOperation == 3:
        #Call Astar and generate plan
        pass  
    

    while 1:
      self.lc.handle()      

      leftVel = 0
      rightVel = 0
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
          command = velocity_cmd_t()
          command.Distance = 987654321.0
          if event.button == 1:
            if ((event.pos[0] > 438) and (event.pos[0] < 510) and
              (event.pos[1] > 325) and (event.pos[1] < 397)):
              command.utime = time.time()
              command.FwdSpeed = 200.0
              command.Distance = 200.0
              command.AngSpeed = 0.0
              command.Angle = 0.0
              print "Commanded PID Forward One Meter!"
            elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
              (event.pos[1] > 400) and (event.pos[1] < 472)):
              command.utime = time.time()
              command.FwdSpeed = 200.0
              command.Distance = -200.0
              command.AngSpeed = 0.0
              command.Angle = 0.0
              print "Commanded PID Backward One Meter!"
            elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
              command.utime = time.time()
              command.FwdSpeed = 0.0
              command.Distance = 0.0
              command.AngSpeed = 35.0
              command.Angle = 45.0
              print "Commanded PID Left One Meter!"
            elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
              command.utime = time.time()
              command.FwdSpeed = 0.0
              command.Distance = 0.0
              command.AngSpeed = -35.0
              command.Angle = -45.0
              print "Commanded PID Right One Meter!"
            elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
              	      (event.pos[1] > 325) and (event.pos[1] < 397)):
              command.utime = time.time()
              pid_cmd = pid_init_t()
              pid_cmd.kp = 0.0        # CHANGE FOR YOUR GAINS!
              pid_cmd.ki = 0.0        # See initialization
              pid_cmd.kd = 0.0
              pid_cmd.iSat = 0.0
              self.lc.publish("GS_PID_INIT",pid_cmd.encode())
              print "Commanded PID Reset!"
            if (command.Distance != 987654321.0):
              	      self.lc.publish("GS_VELOCITY_CMD",command.encode())
              	      command.utime = time.time()
              	      command.FwdSpeed = 0.0
              	      command.Distance = 0.0
              	      command.AngSpeed = 0.0
              	      command.Angle = 0.0
              	      #self.lc.publish("GS_VELOCITY_CMD",command.encode())
              	      

      self.screen.fill((255,255,255))

      # Plot Lidar Scans
      # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
      plt.cla()
      self.ax.plot(0,0,'or',markersize=2)
      self.ax.set_rmax(1.5)
      self.ax.set_theta_direction(-1)
      self.ax.set_theta_zero_location("N")
      self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                    labels=['','','','','','','',''], 
                                    frac=None,fmt=None)
      self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                    angle=None,fmt=None)

      self.ax.plot(self.lidarThetas, self.lidarRanges, 'r',linewidth=2)
      #print max(self.lidarRanges)
    
      canvas = agg.FigureCanvasAgg(self.fig)
      canvas.draw()
      renderer = canvas.get_renderer()
      raw_data = renderer.tostring_rgb()
      size = canvas.get_width_height()
      surf = pygame.image.fromstring(raw_data, size, "RGB")
      self.screen.blit(surf, (320,0))

      # Position and Velocity Feedback Text on Screen
      pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
      text = font.render("  POSITION  ",True,(0,0,0))
      self.screen.blit(text,(10,360))
      xstring = "x: %.3f [mm]" %(self.currOdoPos[0])
      text = font.render(xstring,True,(0,0,0))
      self.screen.blit(text,(10,390))
      ystring = "y: %.3f [mm]" %(self.currOdoPos[1])
      text = font.render(ystring,True,(0,0,0))
      self.screen.blit(text,(10,420))
      tstring = "t: %.3f [deg]" %(self.currOdoPos[2]*180/math.pi)
      text = font.render(tstring,True,(0,0,0))
      self.screen.blit(text,(10,450))
 
      text = font.render("  VELOCITY  ",True,(0,0,0))
      self.screen.blit(text,(150,360))
      dxstring = "dxy/dt: %.3f [mm/s]" %(self.currOdoVel[0]*1000000/(self.currOdoVel[2]+1)) #to avoid division by 0 in the beginning
      text = font.render(dxstring,True,(0,0,0))
      self.screen.blit(text,(150,390))
      dthstring = "dth/dt: %.3f [deg/s]" %(self.currOdoVel[1]*1000000/(self.currOdoVel[2]+1))
      text = font.render(dthstring,True,(0,0,0))
      self.screen.blit(text,(150,420))
      dtstring = "dt: %.3f [Us]" %(self.currOdoVel[2])
      text = font.render(dtstring,True,(0,0,0))
      self.screen.blit(text,(150,450))

      # Plot Buttons
      self.screen.blit(self.arrowup,(438,325))
      self.screen.blit(self.arrowdown,(438,400))
      self.screen.blit(self.arrowleft,(363,400))
      self.screen.blit(self.arrowright,(513,400))
      self.screen.blit(self.resetbut,(513,325))

      pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()

