import math
import lcm
import time
import astar

from lcmtypes import velocity_cmd_t

ANG_TOLERANCE = 5.0 # Degrees
DIST_TOLERANCE = 10.0 # MM
STOP_TOLERANCE = 1e-6 # TOLERANCE TO CHECK POSITION OR ANGLE DIDNT MOVE
STOP_TIMER = 15 # ITERATIONS UP TO CHECK IF POSITION OR ANGLE IS NOT MOVING

class Guidance():

    # INITIALIZE
    # YOU NEED TO CREATE A GUIDANCE OBJECT IN YOU GROUND STATION
    # CURRENT PLAN IS HARD CODED. NEED TO BE RECEIVED FROM YOUR PLANNING
    # JUST A LIST OF LISTS WITH COORDINATES IN MM    
    
    #Comment: Create small steps as otherwise straight motion and turn may overlap
    #Comment: Assumes that once commanded, waypoint WILL be achieved
    def __init__(self):
        self.leg = 0
        
        self.state = -1
        #self.plan = [[0,0],[200,0],[200,200],[0,200],[0,0]]
        #self.plan = [[3000,3000],[3100,3000],[3150,3050],[3100,3100],[3000,3100]]
        self.plan = [ [3150, 3050], [3250, 3050], [3350, 3050], [3450, 3050], [3550, 3050], [3650, 3050], [3750, 3050], [3850, 3050], [3950, 3050], [4050, 3150], [4050, 3250], [4150, 3250], [4250, 3250], [4350, 3250], [4450, 3250], [4550, 3150], [4650, 3150], [4750, 3150], [4850, 3150], [4950, 3050], [5050, 3050], [5150, 2950], [5250, 2950]]
        #self.plan = []
        self.nleg = len(self.plan)
        self.hdg = 0.0
        self.dist = 0.0
        self.command = velocity_cmd_t()
        self.lc = lcm.LCM()
        self.old_dist = 0.0
        self.old_hdg = 0.0
        self.counter = 0
   
        #initialise A-star
        astar.astar_map = astar.Astar()
        astar.AstarMap = astar.astar_map.getAstarMap('map1.png')
        self.goal_position = (5000,3000) 


    # I USE THIS FUNCTION SO I CAN HAVE THE GUIDE AND COMMAND
    # ALWAYS RUNNING IN MY GROUND STATION. WHILE IT IS NOT STARTED IT
    # DOES NOTHING
    def start(self):
        '''
        Starts guidance
        '''
        self.state = 0
    

    def generate_path(self, curr_position):
        '''
        Generates Astar path and starts guidance
        Should be called whenever a path needs to be computed either due to origin or due to deviation
        '''
        #Call A-star to generate path
        print "Generating path from Astar"
        path = astar.astar_map.compute(curr_position, self.goal_position)
        print "Plan generated is ", path
        self.set_plan(path)
        #Restart navigation
        print "Starting navigation"
        self.start()
        
    def set_plan(self, plan):
    	self.plan = [i for i in plan]
    	print "Following plan ", self.plan

    # CORE OF GUIDANCE
    # SIMPLE STATE MACHINE
    # 0 - CALCULATES NEW HEADING AND COMAMNDS IT
    # 1 - WAIT HEADING TO GET TO THE COMMANDED VALUE OR 
    #     DETECTS ROBOT IS NOT MOVING. BOTH CHANGES TO STATE 2
    # 2 - CALCULATES AND COMMAND NEW FORWARD MOTION
    # 3 - WAIT UNTIL NEW POSITION IS ACHIEVED. UPDATES LEG AND
    #     RESET STATE TO 0
    # NEEDS AN A 3D ARRAY WITH X POS, Y POS AND HDG
    # PUBLISHES COMMAND TO PID
    def guide_and_command(self,pos):
    	self.command.utime = time.time()
        pos[0] = 6000 - pos[0] ##CHECK
        pos[2] = - pos[2] ##CHECK
        print "SLAM Output : ", pos
        if (self.state == 0):
            self.hdg = math.atan2((self.plan[self.leg][1]-pos[1]),
                                   (self.plan[self.leg][0]-pos[0]))*180.0/math.pi
            print "pose ", pos #self.old_dist, self.old_hdg, pos[2]
            print "plan", self.plan[self.leg]
            print "plan desired angle", self.hdg
            print "angle correction", -(pos[2]*180.0/math.pi - self.hdg)
            if(math.fabs(pos[2]*180.0/math.pi - self.hdg) < ANG_TOLERANCE):
                self.state = 2
            else:
                self.state = 1 
                self.counter = -1 
                self.command.FwdSpeed = 0.0
                self.command.Distance = 0.0
                self.command.Angle = self.hdg-pos[2]*180.0/math.pi
                if(self.command.Angle > 0):
                    self.command.AngSpeed = 35.0
                else:
                    self.command.AngSpeed = -35.0
                print "State 0 Command :", self.command.AngSpeed, self.command.Angle, "Leg: ", self.leg
                self.lc.publish("GS_VELOCITY_CMD",self.command.encode())
        elif (self.state == 1):
            if (self.counter % STOP_TIMER == 0):
                self.old_hdg = pos[2]
                self.counter = 0
            #if((math.fabs(pos[2]*180.0/math.pi - self.command.Angle) 
            if((math.fabs(pos[2]*180.0/math.pi - self.hdg) 
                < ANG_TOLERANCE) or
                ((self.counter == STOP_TIMER-1) and 
                (math.fabs(self.old_hdg - pos[2])*180.0/math.pi < STOP_TOLERANCE))):
                self.state = 2
                self.command.AngSpeed = 0.0
                self.command.Angle = 0.0
                print "State 1 Command :", self.command.AngSpeed, self.command.Angle, "Leg: ", self.leg
                #self.lc.publish("GS_VELOCITY_CMD",self.command.encode())
        elif (self.state == 2):
            self.dist = math.sqrt((self.plan[self.leg][1]-pos[1])*
                         (self.plan[self.leg][1]-pos[1]) + 
                         (self.plan[self.leg][0]-pos[0])*
                         (self.plan[self.leg][0]-pos[0]))
            #####
            #if self.dist > 200: #Should be a multiple of hole size in mm
            #    self.generate_path(pos, self.goal_position)
            #    return
            #####

            if (self.dist < DIST_TOLERANCE):
                self.leg += 1
                if(self.leg < self.nleg):
                    self.state = 0
                else:
                    self.state = -1
            else:
                self.state = 3
                self.counter = -1
                self.command.FwdSpeed = 200.0
                self.command.Distance = self.dist
                print "State 2 Command :", self.command.FwdSpeed, self.command.Distance, "Leg: ", self.leg
                self.lc.publish("GS_VELOCITY_CMD",self.command.encode())
        elif (self.state == 3):
            if (self.counter % STOP_TIMER == 0):
                self.old_dist = self.dist
                self.counter = 0
            self.dist = math.sqrt((self.plan[self.leg][1]-pos[1])*
                         (self.plan[self.leg][1]-pos[1]) + 
                         (self.plan[self.leg][0]-pos[0])*
                         (self.plan[self.leg][0]-pos[0]))
            if ((self.dist < DIST_TOLERANCE) or
                ((self.counter == STOP_TIMER-1) and 
                (math.fabs(self.dist - self.old_dist) < STOP_TOLERANCE))):
                self.command.FwdSpeed = 0.0
                self.command.Distance = 0.0
                print "State 3 Command :", self.command.FwdSpeed, self.command.Distance, "Leg: ", self.leg 
                #self.lc.publish("GS_VELOCITY_CMD",self.command.encode())         
                self.leg += 1
                if(self.leg < self.nleg):
                    self.state = 0
                else:
                    self.state = -1
        self.counter += 1
        
        #print "-------GUIDANCE----------"
        #print "Dist, Hdg, State, Leg ", self.dist, self.hdg, self.state, self.leg
        #print "pose ", self.pos #self.old_dist, self.old_hdg, pos[2]
        #print "plan", self.plan[self.leg]
        #print "Command dist,angle,omega ", self.command.Distance, self.command.Angle, self.command.AngSpeed
        #print "-------------------------"
