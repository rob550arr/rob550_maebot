# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time,signal
import lcm
import math 
import PID
import time
#import matplotlib.pyplot as plt

from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
#from lcmtypes import maebot_command_t

from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t


class PID():
    def __init__(self, kp, ki, kd):
        # Main Gains
        self.kp = kp
        self.ki = ki
        self.kd = kd
            
        # Integral Terms
        self.iTerm = 0.0
        self.iTermMin = -0.2
        self.iTermMax = 0.2

        # Input (feedback signal)
        self.prevInput = 0.0
        self.input = 0.0

        # Reference Signals (signal and derivative!)
        self.dotsetpoint = 0.0          
        self.setpoint = 0.0

        # Output signal and limits
        self.output = 0.0
        self.outputMin = -0.7 # changed now
        self.outputMax = 0.7  # changed now

        # Update Rate
        self.updateRate = 0.05 #In seconds # Changed now from 0.04

        # Error Signals
        self.error = 0.0
        self.errordot = 0.0
        self.preverror = 0.0
        self.prevSetpoint = 0.0 #Not sure if needed

    def ResetITerm(self):
        self.iTerm=0
        self.setpoint=0
        
        
    def Compute(self, dotsetpoint, currInput, trim): #Desired Velocity (user input), actual distance (feedback)
        # IMPLEMENT ME!
        # Different then last project! 
        # Now you have a speed reference also!
        # Also Update Rate can be gone here or in SetTunnings function
        # (it is your choice!) But change functions consistently
        
        self.input = currInput
        self.setpoint += dotsetpoint*self.updateRate
        self.dotsetpoint = dotsetpoint
        #print" Setpoint:", self.setpoint, " Actual Distance : ", currInput
        
        self.error = self.setpoint - self.input
        #print "error ", self.error
                
#self.errordot = self.dotsetpoint - (self.input - self.prevInput) #CHANGED NOW Not sure if this is the appropriate way
       # self.errordot=self.ScaleNegativeFloat(self.errordot, -100,100) #changed now
        self.error=self.ScaleNegativeFloat(self.error, -100,100)
        self.errordot = self.error - self.preverror #added now
        self.preverror = self.error #added now
       # self.errordot=self.ScaleNegativeFloat(self.errordot, -dotsetpoint*self.updateRate,dotsetpoint*self.updateRate)
       # self.error=self.ScaleNegativeFloat(self.error,0,dotsetpoint*self.updateRate)
        pTerm = self.kp*self.error
        self.iTerm += self.ki*self.error
        dTerm = self.kd*self.errordot
        
        if(self.iTerm<self.iTermMin): self.iTerm = self.iTermMin
        if(self.iTerm>self.iTermMax): self.iTerm = self.iTermMax
        
        if(dotsetpoint<0): trim = -trim #trim will contribute towards direction of motion
        if(dotsetpoint>0): trim = trim  #therefore trim must be passed as magnitude!
        
        self.output = pTerm + self.iTerm + dTerm + trim 
        #print "Scaled Error: ",self.error, "ErrorDot: ", self.errordot
        #print "P:",pTerm, "I",self.iTerm,"D", dTerm
        #print "Scaled Error: ",self.error
        #print "P:",pTerm
        
        if(self.output<self.outputMin): self.output = self.outputMin
        if(self.output>self.outputMax): self.output = self.outputMax
        #if(self.output<self.outputMin): self.output = self.outputMin
        #if(self.output>self.outputMax): self.output = self.outputMax
        
       
        #print "Motor Output: ", self.output
        
        self.prevInput = self.input
        self.prevSetpoint = self.setpoint
       
    # Accessory Function to Change Gains
    # Update if you are not using updateRate in Compute()
    def SetTunings(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki*self.updateRate
        self.kd = kd/self.updateRate

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
                
    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec

    def ScaleNegativeFloat(self, currVal, currMin, currMax):
       if currVal>currMax: return 1
       if currVal<currMin: return -1
       currMean = (currMin + currMax)/2
       return (currVal-currMean)/(currMax)  
       
class PIDController():
    def __init__(self):
        
        print "--TEAM 11--"

        # Create Both PID
        self.leftCtrl =  PID(0.0,0.0,0.0)
        self.rightCtrl = PID(0.0,0.0,0.0)
        
        #self.leftCtrl.SetIntegralLimits()
        self.leftCtrl.SetOutputLimits(-0.6, 0.6)
        #self.leftCtrl.SetUpdateRate()              
        #self.rightCtrl.SetIntegralLimits()
        self.rightCtrl.SetOutputLimits(-0.6, 0.6)
        #self.rightCtrl.SetUpdateRate()
        self.leftCtrl.SetTunings(0.60,0.0,0.0) # changed now
        self.rightCtrl.SetTunings(0.60,0.0,0.0) # changed now
        #self.leftCtrl.SetTunings(0.60,0.1,0.08)
        #self.rightCtrl.SetTunings(0.60,0.1,0.08)
        
 
        # LCM Subscribe
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
        lcmPoseSub = self.lc.subscribe("ODO_POSE_XYT", self.odoPoseHandler)
        lcmVelSub = self.lc.subscribe("ODO_DXDTHETA", self.odoVelHandler)
        lcmCommandSub = self.lc.subscribe("GS_VELOCITY_CMD", self.motorCommandHandler)
        lcmCommandSub = self.lc.subscribe("GS_PID_INIT", self.PIDInitHandler)
        signal.signal(signal.SIGINT, self.signal_handler)

        # Odometry 
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / 
                       (self.gearRatio * self.ticksPerRev)) 
                        # encoder ticks to distance [mm]

        # Initialization Variables
        self.InitialPosition = (0.0, 0.0)
        self.oldTime = 0.0
        self.startTime = 0.0
        #self.counter = 0
        self.prevCommandTime = 0
        self.command = (0,0,0,0,0)
        #Position and Velocity Variables
        self.currOdoPos = (0, 0, 0)
        self.currOdoVel = (0, 0, 0)
        self.currWheelPos = (0, 0, 0)
        self.currEncPos = (0,0,0)
        
        
        # def resetPID(self):
    	# Create Both PID
        #self.leftCtrl =  PID(0.0,0.0,0.0)
        #self.rightCtrl = PID(0.0,0.0,0.0)
        
        #self.leftCtrl.SetIntegralLimits()
        #self.leftCtrl.SetOutputLimits(-0.7, 0.7)
        #self.leftCtrl.SetUpdateRate()              
        #self.rightCtrl.SetIntegralLimits()
        #self.rightCtrl.SetOutputLimits(-0.7, 0.7)
        #self.leftCtrl.SetTunings(0.60,0.0,0.0) # changed now for turn
        #self.rightCtrl.SetTunings(0.60,0.0,0.0) # changed now for turn
        #self.rightCtrl.SetUpdateRate()
        #self.leftCtrl.SetTunings(0.70,0.18,0.09)
        #self.rightCtrl.SetTunings(0.70,0.18,0.09)
        
    	    
    	    
    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        cmd.motor_left_speed = self.leftCtrl.output 
        cmd.motor_right_speed = self.rightCtrl.output
        #print "Motor outputs:", cmd.motor_left_speed, cmd.motor_right_speed
        self.lc.publish("MAEBOT_DIFF_DRIVE", cmd.encode())
        
        
   

    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        self.prevEncPos = self.currEncPos
        self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
        if self.prevEncPos == (0,0,0):
            return
        self.prevWheelPos = self.currWheelPos 
        self.currWheelPos = (self.currWheelPos[0] + self.enc2mm*(self.currEncPos[0]-self.prevEncPos[0]),
        	             self.currWheelPos[1] + self.enc2mm*(self.currEncPos[1]-self.prevEncPos[1]),
        	             self.currEncPos[2])

        #self.local_xref = self.local_xref + self.DesiredSpeed*self.leftCtrl.updateRate #only for plot
        #self.plotGraph(self.local_xref,self.currWheelPos[0],self.currWheelPos[1],self.currWheelPos[2])
        #print "utime", self.currWheelPos[2]

	#print "Current Distance: ", self.currWheelPos[0], self.currWheelPos[1]
        #print "Velocity Estimate: %.2f %.2f" %((self.currWheelPos[0] - self.prevWheelPos[0])/self.leftCtrl.updateRate,
        	#(self.currWheelPos[1] - self.prevWheelPos[1])/self.rightCtrl.updateRate) 
        
        # IMPLEMENT ME
    
    def odoPoseHandler(self, channel, data):
        msg = odo_pose_xyt_t.decode(data)
        self.currOdoPos = (msg.xyt[0], msg.xyt[1], msg.xyt[2], msg.utime)
            
    def odoVelHandler(self, channel, data):
        msg = odo_dxdtheta_t.decode(data)
        self.currOdoVel = (msg.dxy, msg.dtheta, msg.dt)

    def motorCommandHandler(self,channel,data):
        msg = velocity_cmd_t.decode(data)
        # IMPLEMENT ME
        # Change to Receive the Message from Ground Station
        # And command the bot
        self.command = (msg.utime, msg.FwdSpeed, msg.Distance, msg.AngSpeed, msg.Angle)
        print "NEW COMMAND!"
        print "command packet (utime, speed, dist, omega, angle)", self.command
    
    def PIDInitHandler(self,channel,data):
        msg = pid_init_t.decode(data)
        #Implement if needed

    def Controller(self):

        # For now give a fixed command here
        # Later code to get from groundstation should be used
        
        #Later on - flow of control will be - ground station -> outer loop controller -> inner loop controller
        #self.go_straight_test()                     
        self.currWheelPos = (0,0,0)
        
        self.DesiredSpeed = 0 #Do not set!
        self.DesiredDistance = 0
        targetSpeed = 100
        self.start_angle = 0
    	oldTime = time.time()
        #self.plotInit()
        #for i in range(0,2,1):  # changed now
                      
        #   self.leftCtrl.output=0
        #   self.rightCtrl.output=0
        #   self.publishMotorCmd()
        #   
        #   print"Going straight ", i
        #   self.currWheelPos = (0,0,0) #IS THIS NEEDED?
           #time.sleep(1)
        #   self.go_straight_test(targetSpeed, 1000)
        #   self.leftCtrl.ResetITerm()
        #   self.rightCtrl.ResetITerm()           
           
        #   print "turning i", i
        #   self.currWheelPos = (0,0,0) #IS THIS NEEDED?
           
        #   self.turn_test(90.0)
        #   self.leftCtrl.ResetITerm()
        #   self.rightCtrl.ResetITerm()
        
        #self.command = (msg.utime, msg.FwdSpeed, msg.Distance, msg.AngSpeed, msg.Angle)
        while 1:
            self.lc.handle()
            #print "Looping" 
            if self.command[0] != self.prevCommandTime: 
                print "Command", self.command
                self.prevCommandTime = self.command[0]
                #if self.command[0] != self.prevCommandTime: #Redundant
                if self.command[4] != 0:
                    self.leftCtrl.ResetITerm()
                    self.rightCtrl.ResetITerm()
                	#self.start_angle += self.command[4]%180
                    print "Turning by ", self.command[4]
                    self.turn_test(self.command[4])
                    self.leftCtrl.output=0
                    self.rightCtrl.output=0
                    self.publishMotorCmd()
                    self.leftCtrl.ResetITerm()
                    self.rightCtrl.ResetITerm()

                if self.command[2] != 0:
                    if self.command[2]>0:
                        print "Going forward by ", self.command[2]
                        self.go_straight_test(self.command[1], int(self.command[2]))
                        self.leftCtrl.output=0
                        self.rightCtrl.output=0
                        self.publishMotorCmd()
                        self.leftCtrl.ResetITerm()
                        self.rightCtrl.ResetITerm()
                    if self.command[2]<0:
                        print "Going backward by ", self.command[2]
                        self.go_straight_test(-self.command[1], -int(self.command[2]))
                        self.leftCtrl.output=0
                        self.rightCtrl.output=0
                        self.publishMotorCmd()
                        self.leftCtrl.ResetITerm()
                        self.rightCtrl.ResetITerm()
        return                
          
    def turn_test(self, targetTheta): #number of degrees to cover - ideally max 180 - with respect to initial angle
    	    
        self.DesiredSpeed = 50.0
        
       # angle = targetTheta/2-self.currOdoPos[2]*180/math.pi # changed now
        angle = targetTheta
        self.DesiredDistance = abs(angle/180.0)*math.pi*self.axleLengthMillimeters/2 - self.DesiredSpeed*self.leftCtrl.updateRate - 10 #Remove this diff
        self.startingWheelPos = self.currWheelPos
        
        
        if angle>0: direction=-1 #SWAPPED - CHECK?
        elif angle<0: direction=1
        
    	oldTime = time.time()
        print "Desired Distance and angle", self.DesiredDistance, targetTheta
        self.leftCtrl.SetTunings(0.60,0.1,0.0)
        self.rightCtrl.SetTunings(0.60,0.1,0.0)
        
        while((abs(self.currWheelPos[0]-self.startingWheelPos[0])<self.DesiredDistance) or 
              (abs(self.currWheelPos[1]-self.startingWheelPos[1])<self.DesiredDistance)):
            self.lc.handle()
            #print "Previous Value Left", self.prevEncPos[0], "Current Value", self.currEncPos[0], "CurrWheelPos", self.currWheelPos[0]
            #print "Previous Value Right", self.prevEncPos[1], "Current Value", self.currEncPos[1], "CurrWheelPos", self.currWheelPos[1]
            #print ""
            #print "Time taken", time.time()-oldTime
            #print "CurrWheelPos" , self.currWheelPos
            if(time.time()-oldTime > self.leftCtrl.updateRate):
              #print "CurrWheelPosEnter" , self.currWheelPos[0]	    
              if(abs(self.currWheelPos[0]-self.startingWheelPos[0])<self.DesiredDistance): 
              	      #print "L", self.currWheelPos[0]-self.startingWheelPos[0]
              	      self.leftCtrl.Compute(direction*self.DesiredSpeed, abs(self.currWheelPos[0]-self.startingWheelPos[0]), 0.1)
              else:
              	      self.leftCtrl.output=0
              if(abs(self.currWheelPos[1]-self.startingWheelPos[1])<self.DesiredDistance): 
              	      #print "R", self.currWheelPos[1]-self.startingWheelPos[1]
                      self.rightCtrl.Compute(-direction*self.DesiredSpeed, abs(self.currWheelPos[1]-self.startingWheelPos[1]), 0.1)
              else:
              	      self.rightCtrl.output=0
              oldTime = time.time()
              
              # IMPLEMENT ME
              # MAIN CONTROLLER
            self.publishMotorCmd()
      
        return
    	
        
    def go_straight_test(self, targetSpeed, targetdistance): #Target distance is relative, not absolute. 
        #self.DesiredSpeed = targetSpeed # changed now
        self.DesiredSpeed = 0 # changed now
        self.DesiredDistance = targetdistance
        self.startingWheelPos = self.currWheelPos
        self.MinDesiredSpeed = targetSpeed/4 # changed now
        self.MaxDesiredSpeed = targetSpeed
        self.brakingDistance = 100 # changed now
        
        oldTime = time.time()
        #self.plotInit()
        self.local_xref = 0
        self.leftCtrl.SetTunings(0.70,0.18,0.09)
        self.rightCtrl.SetTunings(0.70,0.18,0.09)
        
        #print "GO STRAIGHT TEST"
        
        
        while((abs(self.currWheelPos[0]-self.startingWheelPos[0])<self.DesiredDistance or 
               abs(self.currWheelPos[1]-self.startingWheelPos[1])<self.DesiredDistance)):
            self.lc.handle()
            #print "PID Loop"
            
            if (abs(self.DesiredSpeed) < abs(self.MaxDesiredSpeed)): # changed now
              self.DesiredSpeed = (self.MaxDesiredSpeed - self.MinDesiredSpeed)*((abs(self.currWheelPos[0]-self.startingWheelPos[0])+abs(self.currWheelPos[1]-self.startingWheelPos[1]))/2)/self.brakingDistance + self.MinDesiredSpeed # changed now	    
              #print "Accelerating to ", self.DesiredSpeed              
              #self.DesiredSpeed = 2.4 * ((self.currWheelPos[0]+self.currWheelPos[1])/2) + 50    # changed now
                     
            if (abs(self.DesiredSpeed) >= abs(self.MaxDesiredSpeed)): # changed now
              self.DesiredSpeed = targetSpeed # changed now
              #print "Maintaining ", self.DesiredSpeed
              
            if(time.time()-oldTime > self.leftCtrl.updateRate):
              if(abs(self.currWheelPos[0]-self.startingWheelPos[0])<self.DesiredDistance): 
                #print "L"
                self.leftCtrl.Compute(self.DesiredSpeed, self.currWheelPos[0]-self.startingWheelPos[0], 0.3) # changed now                   
              if(abs(self.currWheelPos[1]-self.startingWheelPos[1])<self.DesiredDistance): 
           	#print "R"
                self.rightCtrl.Compute(self.DesiredSpeed, self.currWheelPos[1]-self.startingWheelPos[1], 0.3)
              oldTime = time.time()
	      # IMPLEMENT ME
              # MAIN CONTROLLER
            self.publishMotorCmd()
        return
       
    # Function to print 0 commands to morot when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0  
            self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        exit(1)

pid = PIDController()
pid.Controller()