# odometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

class Maebot(WheeledRobot):
    
  def __init__(self):
    self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
    self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
    self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
    self.gearRatio = 30.0                    # 30:1 gear ratio
    self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]
    self.counter=0;
    self.prevEncPos = (0,0,0)           # store previous readings for odometry
    self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
    self.currEncPos = (0,0,0)           # current reading for odometry
    self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
    self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
    self.prevGyro = (0,0,0,0)
    self.currGyro = (0,0,0,0)
    self.biasGyro = (0,0,0)#(-339, 32, -37)
    self.zeroGyro = (0,0,0)
    self.prevRawGyro = (0,0,0)
    self.currRawGyro = (0,0,0) #Only for calibration
    self.sgyro = [0,0,0,0] # for gyro bias calibration
    
    WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

    # LCM Initialization and Subscription
    self.lc = lcm.LCM()
    lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA", self.sensorDataHandler)
    #self.gyro_calibration()
    lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler) # to read from a channel
    
  
  def calcVelocities(self):
    # IMPLEMENT ME
    # TASK: CALCULATE VELOCITIES FOR ODOMETRY
    # Update self.currOdoVel and self.prevOdoVel
    dxy = self.enc2mm*((self.currEncPos[0]-self.prevEncPos[0]) + (self.currEncPos[1]-self.prevEncPos[1]))/2 # changed
    dtheta = self.enc2mm*((self.currEncPos[1]-self.prevEncPos[1]) - (self.currEncPos[0]-self.prevEncPos[0]))/self.axleLengthMillimeters
    dt = self.currEncPos[2]-self.prevEncPos[2]
    self.currOdoVel = (dxy, dtheta*180.0/pi, dt)

  def getVelocities(self):
    # IMPLEMENT ME
    # TASK: RETURNS VELOCITY TUPLE
    dxy = self.currOdoVel[0]
    dtheta = self.currOdoVel[1]  
    dt = self.currOdoVel[2]
    
    # Return a tuple of (dxy [mm], dtheta [rad], dt [s])
    return (dxy, dtheta, dt) # [mm], [rad], [s]

  def calcOdoPosition(self):
    # IMPLEMENT ME
    # TASK: CALCULATE POSITIONS
    # Update self.currOdoPos and self.prevOdoPos
    self.prevOdoPos = self.currOdoPos
    dxy = self.enc2mm*((self.currEncPos[0]-self.prevEncPos[0]) + (self.currEncPos[1]-self.prevEncPos[1]))/2 # changed
    dtheta = self.enc2mm*((self.currEncPos[1]-self.prevEncPos[1]) - (self.currEncPos[0]-self.prevEncPos[0]))/self.axleLengthMillimeters
    dtheta = self.gyroOdometry(dtheta)
    theta = self.prevOdoPos[2] + dtheta 
    if theta > pi : theta = theta - 2*pi
    if theta <-pi : theta = theta + 2*pi
    self.currOdoPos = ( self.prevOdoPos[0] + dxy*cos(self.prevOdoPos[2]), # changed
    			self.prevOdoPos[1] + dxy*sin(self.prevOdoPos[2]), # changed
                        theta )
    
    
  def getOdoPosition(self):
    # IMPLEMENT ME
    # TASK: RETURNS POSITION TUPLE
    # Return a tuple of (x [mm], y [mm], theta [rad])
    x = self.currOdoPos[0] 
    y = self.currOdoPos[1] 
    theta = self.currOdoPos[2] 
    return (x, y, theta) # [mm], [rad], [s]

  def publishOdometry(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_POSE MESSAGE
    msg = odo_pose_xyt_t() #changed
    msg.xyt[0] =  self.currOdoPos[0] #changed
    msg.xyt[1] =  self.currOdoPos[1] #changed
    msg.xyt[2] =  self.currOdoPos[2] #changed 
    msg.utime = self.currEncPos[2] #changed again
    self.lc.publish("ODO_POSE_XYT", msg.encode()) #changed

 
  
  def publishVelocities(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
    msg = odo_dxdtheta_t() 
    msg.dxy = int(self.currOdoVel[0])
    msg.dtheta = int(self.currOdoVel[1])
    msg.dt = self.currOdoVel[2]
    msg.utime = self.currEncPos[2]
    print "msg dxy, dt, dtheta", msg.dxy, msg.dtheta, msg.dt
    self.lc.publish("ODO_DXDTHETA", msg.encode()) #changed
    
  def motorFeedbackHandler(self,channel,data):
    msg = maebot_motor_feedback_t.decode(data)
    self.prevEncPos = self.currEncPos
    self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
    if self.prevEncPos == (0,0,0):
      return
    self.calcOdoPosition()
    self.calcVelocities()
          
    # IMPLEMENT ME
    # TASK: PROCESS ENCODER DATA
    # get encoder positions and store them in robot,
    # update robots position and velocity estimate

  def sensorDataHandler(self,channel,data):
    
    msg = maebot_sensor_data_t.decode(data)
    
    self.prevRawGyro = self.currRawGyro
    self.currRawGyro = (msg.gyro[0], msg.gyro[1], msg.gyro[2])
    
    self.prevGyro = self.currGyro
    
    
    
    if self.counter == 0 :
    	currGyro = [(msg.gyro_int[0])/(131*1000000)-self.biasGyro[0]-self.zeroGyro[0], 
                (msg.gyro_int[1])/(131*1000000)-self.biasGyro[1]-self.zeroGyro[1], 
                (msg.gyro_int[2])/(131*1000000)-self.biasGyro[2]-self.zeroGyro[2],
                 msg.utime]
        #if currGyro[0] > 180 : currGyro[0] = currGyro[0] - 360
        #if currGyro[0] <-180 : currGyro[0] = currGyro[0] + 360
        #if currGyro[1] > 180 : currGyro[1] = currGyro[1] - 360
        #if currGyro[1] <-180 : currGyro[1] = currGyro[1] + 360
        #if currGyro[2] > 180 : currGyro[2] = currGyro[2] - 360
        #if currGyro[2] <-180 : currGyro[2] = currGyro[2] + 360        
        self.currGyro = (currGyro[0], currGyro[1], currGyro[2], currGyro[3])
        self.counter = 1
        self.zeroGyro = self.currGyro 
        print "Zerogyro: " , self.zeroGyro
        
    else :
        currGyro = [(msg.gyro_int[0])/(131*1000000)-self.biasGyro[0]-self.zeroGyro[0], 
                (msg.gyro_int[1])/(131*1000000)-self.biasGyro[1]-self.zeroGyro[1], 
                (msg.gyro_int[2])/(131*1000000)-self.biasGyro[2]-self.zeroGyro[2],
                 msg.utime]
        currGyro[2] = currGyro[2]*0.8  
        if currGyro[2] > 180 :
          currGyro[2] = currGyro[2] - 360
        if currGyro[2] <-180 :
          currGyro[2] = currGyro[2] + 360
        self.currGyro = (currGyro[0], currGyro[1], currGyro[2], currGyro[3])
        self.biasGyro = (self.biasGyro[0]+(self.currGyro[3]-self.prevGyro[3])*(-362.0)/(131*1000000),
                         self.biasGyro[1]+(self.currGyro[3]-self.prevGyro[3])*(+100.0)/(131*1000000),
                         self.biasGyro[2]+(self.currGyro[3]-self.prevGyro[3])*(32.0)/(131*1000000)) 

    #print "Gyro1: ", self.currGyro[0], self.currGyro[1], self.currGyro[2]
    #print "Gyro1: ", self.biasGyro[0], self.biasGyro[1], self.biasGyro[2]
    #print "Gyro1: ", self.zeroGyro[0], self.zeroGyro[1], self.zeroGyro[2]
    #print "Gyro1: ", msg.gyro[0], msg.gyro[1], msg.gyro[2]	
    #print "Gyro1: ", self.currGyro[3]-self.prevGyro[3]
    #print "Gyro1: ", self.counter
    	
    # IMPLEMENT ME
    # TASK: PROCESS GYRO DATA
  def gyroOdometry(self,dthetaOdo):
    if abs((self.currGyro[2]-self.prevGyro[2])-dthetaOdo*180/3.14)> 6000:#0.6 :
	print "Gyro used"
	#print "Dgyro: ", self.currGyro[2]-self.prevGyro[2]
	#print "Dodo: ", dthetaOdo*180/3.14
	return (self.currGyro[2]-self.prevGyro[2])*pi/180
		
    else :
        #print "Odo Here"
        #print "Dgyro: ", self.currGyro[2]-self.prevGyro[2]
        #print "Dodo: ", dthetaOdo*180/3.14
        return dthetaOdo #dhetaOdo is in radians while dtheta gyro is in degrees
  
  def gyro_calibration(self):
    oldTime = time.time()
    frequency = 20

    print "Gyro calibration starting. DO NOT MOVE!!"
    while(self.sgyro[3]<6000):
      self.lc.handle()
#      if(time.time()-oldTime > 1.0/frequency):
      self.sgyro[3] += 1
      self.sgyro[0] = ((self.sgyro[3]-1)*self.sgyro[0] + self.currRawGyro[0]) / self.sgyro[3]
      self.sgyro[1] = ((self.sgyro[3]-1)*self.sgyro[1] + self.currRawGyro[1]) / self.sgyro[3]
      self.sgyro[2] = ((self.sgyro[3]-1)*self.sgyro[2] + self.currRawGyro[2]) / self.sgyro[3]
      oldTime = time.time()
      #print "self.sgyro", self.sgyro
    print " Gyro Bias : ", self.sgyro
    self.biasGyro = (self.sgyro[0],self.sgyro[1],self.sgyro[2])
    print " Gyro bias calibration complete "
    print " Gyro bias set to ", self.biasGyro
  
  def MainLoop(self):
    counter = 0
    oldTime = time.time()
    frequency = 20
    
    #Calibrate    
    
    
    #Run
    while(1):
      self.lc.handle()
      if(time.time()-oldTime > 1.0/frequency):
        #print "encoder agg: ", self.currEncPos
        #print "encoder:", self.currEncPos[0] - self.prevEncPos[0], self.currEncPos[1] - self.prevEncPos[1]
        #print "odometry:", self.getOdoPosition()
        #print "velocity:", self.getVelocities()
        self.publishOdometry()
        self.publishVelocities()
        oldTime = time.time()
         


if __name__ == "__main__":
  
  robot = Maebot()
  robot.MainLoop()  
